import os
import re
forbidden_letters_raw = ""
must_contain_raw = ""
while 1:
    known_word_thus_far = input("known word thus far (use \".\" for unknown character, if nothing is known leave empty): ")
    print(known_word_thus_far)
    forbidden_letters_raw = forbidden_letters_raw + input("forbidden letters, no separation: ")
    forbidden_letters = ','.join(forbidden_letters_raw[i:i + 1] for i in
            range(0, len(forbidden_letters_raw), 1))
    print(forbidden_letters)
    must_contain_raw = must_contain_raw + input("definitely contained letters, no separation: ")
    must_contain = ','.join(must_contain_raw[i:i + 1] for i in range(0,
        len(must_contain_raw), 1))
    print(must_contain)
    # formatter must contain: 
    search_string = ","
    replace_text = ".*)(.*"
    must_contain = must_contain.replace(search_string, replace_text)
    # print(must_contain)
    # forbidden_letters = []
    # while 1:
    #     new_letter = input("forbidden letter (empty for stop): ")
    #     if new_letter != "":
    #         forbidden_letters.append(new_letter)
    #     else: break
    # print(forbidden_letters)
    # must_contain = []
    # while 1:
    #     new_letter = input("definitely contains this letter (empty for stop): ")
    #     if new_letter != "":
    #         must_contain.append(new_letter)
    #     else: break

    # print("sed -n -e '/^"+str(known_word_thus_far) + "$/Ip' words.txt | sed -n
    #         -e '/.*[" + str(must_contain) + "].*/Ip'")
    if known_word_thus_far == "":
        known_word_thus_far = "....."
    if forbidden_letters == "":
        forbidden_letters = "0"
    if must_contain == "":
        must_contain = "."
    print("all possible matches: ")
    os.system("sed -n -e '/^"+str(known_word_thus_far) + "$/Ip' words.txt | sed -n -e '/.*[" + str(forbidden_letters) + "].*/I!p' | sed -n -E '/(.*" + str(must_contain) + ".*)/Ip'")
